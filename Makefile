.PHONY: install run test

install:
	poetry install

run: install
	poetry run uvicorn responder.app:app --reload

test: install
	poetry run pytest