Responder
===

Prequisites
---

- `make`
- `poetry` installed ([official instructions](https://python-poetry.org/docs/#installation))

Usage
---

To run the application locally, simply execute `make run`. Auto-reload is enabled for convenience.

You can test the application using:

```bash
curl -X POST http://localhost:8000/question -d '{"question": "some_question", "id": "someid"}' -H "Content-Type: application/json"
```

Documentation
---

After running the application, Swagger UI can be found at [http://localhost:8000/docs](http://localhost:8000/docs).

Testing
---

Tests are located in `responder/tests/`, you can run them using `make test`.
