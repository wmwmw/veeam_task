from fastapi.testclient import TestClient
from fastapi import Response
import pytest

from ..app import app
from ..serializers import MAX_QUESTION_SIZE


@pytest.fixture
def client():
    return TestClient(app)


def test_valid_request(client: TestClient):
    response: Response = client.post(
        "/question", json={"id": "someid", "question": "some_question"}
    )
    assert response.status_code == 200

    data = response.json()
    assert "answer" in data
    assert data["answer"]


@pytest.mark.parametrize(
    "request_json,response_json",
    [
        (
            {"question": "some_question"},
            {
                "detail": [
                    {
                        "loc": ["body", "id"],
                        "msg": "field required",
                        "type": "value_error.missing",
                    }
                ]
            },
        ),
        (
            {"id": "someid", "question": "a" * (MAX_QUESTION_SIZE + 1)},
            {
                "detail": [
                    {
                        "loc": ["body", "question"],
                        "msg": "Question should be at most 512 characters long.",
                        "type": "assertion_error",
                    }
                ]
            },
        ),
    ],
)
def test_invalid_questions(client: TestClient, request_json, response_json):
    response: Response = client.post("/question", json=request_json)
    assert response.status_code == 422

    assert response.json() == response_json
