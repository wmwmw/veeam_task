import random
import string

from fastapi import FastAPI

from .serializers import Question, Answer, MAX_ANSWER_SIZE

app = FastAPI()


def random_string(length: int):
    return "".join(random.choices(string.ascii_letters, k=random.randint(1, MAX_ANSWER_SIZE)))


@app.post("/question")
async def question(item: Question) -> Answer:
    return Answer(answer=random_string(MAX_ANSWER_SIZE))
