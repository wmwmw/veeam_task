import pydantic

MAX_QUESTION_SIZE = 512
MAX_ANSWER_SIZE = 1024


class Question(pydantic.BaseModel):
    id: str
    question: str

    @pydantic.validator("question")
    def validate_question_max_length(cls, v):
        assert (
            len(v) <= MAX_QUESTION_SIZE
        ), f"Question should be at most {MAX_QUESTION_SIZE} characters long."
        return v


class Answer(pydantic.BaseModel):
    answer: str

    @pydantic.validator("answer")
    def validate_answer_max_length(cls, v):
        assert (
            len(v) <= MAX_ANSWER_SIZE
        ), f"Answer should be at most {MAX_ANSWER_SIZE} characters long."
        return v
